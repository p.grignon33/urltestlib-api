from typing import Dict
from urltestlib.report import Report
import boto3
import sys
from dotenv import load_dotenv
import os
from pathlib import Path
import json

from fastapi import FastAPI

app = FastAPI()

# Connect to s3 bucket
env_path = Path(".") / ".env"
load_dotenv(dotenv_path=env_path)
AWS_ACCESS_KEY_ID = os.getenv("AWS_ACCESS_KEY_ID")
AWS_SECRET_ACCESS_KEY = os.getenv("AWS_SECRET_ACCESS_KEY")

bucket_name = "nfinite-api-dump"

s3 = boto3.resource(
    service_name="s3",
    region_name="eu-west-3",
    aws_access_key_id=AWS_ACCESS_KEY_ID,
    aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
)

# Create bucket if it does not exist
if bucket_name not in [bucket.name for bucket in s3.buckets.all()]:
    s3.create_bucket(
        Bucket=bucket_name,
        CreateBucketConfiguration={"LocationConstraint": "eu-west-3"},
    )
    print("Bucket created !")
else:
    print("Bucket already existing !")


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.post("/check-urls")
def check_urls(url_dict: Dict):
    if "urls" in url_dict.keys():
        report = Report(url_dict["urls"])
        filename = report.check_urls_to_bucket_s3(s3, bucket_name)
        bucket = s3.Bucket(bucket_name)
        obj = bucket.Object(filename).get()
        data = json.loads(obj["Body"].read())["report"]
        return data
    else:
        return "Wrong request payload, there is no key named urls !"


@app.get("/all-reports")
def get_all_reports():
    bucket = s3.Bucket(bucket_name)
    reports = [
        f.key
        for f in bucket.objects.all()
        if ((f.key.startswith("report")) and (f.key.endswith(".json")))
    ]

    final_report = {}
    for report in reports:
        obj = bucket.Object(report).get()
        data = json.loads(obj["Body"].read())["report"]
        final_report[report] = data

    return final_report
