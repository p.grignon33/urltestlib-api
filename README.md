# URLTESTLIB-API

urltestlib-api is an API built aroung the Python library urltestlib, which can be used to test the reachability of urls and generate reports.

## Getting started
Clone the git repository, and create a .env file.
Within this file, you will have to define two variables in order to save the data in a s3 bucket.
These variables are:
    - AWS_ACCESS_KEY_ID
    - AWS_SECRET_ACCESS_KEY

When this is done, you can start the API by using:

```bash
uvicorn main:app
```

The different available endpoint are:

    - [GET] / : Simple health check endpoint

    - [POST] /check-urls : checks the url reachability and saves the report in an s3 bucket

    - [GET] /all-reports : returns all the reports stored within the s3 bucket

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Licence

[MIT](https://choosealicense.com/licenses/mit/)

## Go even further

I don't think that the API model I developed is still valid for a very large number of urls, as each the urls are analyzed sequentially. I think that adding some concurrency within the program could really speed up the process.
This could done by applying some multi-threading (multiple threads running at the same time) and multiprocessing (running the different threads on the different CPU cores).

Another solution could be to implement distributed calculations, by using pyspark.
